FROM node

WORKDIR /app

COPY . /app

USER root

CMD node webapp.js

EXPOSE 8888